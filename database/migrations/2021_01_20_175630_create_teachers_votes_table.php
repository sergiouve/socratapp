<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers_votes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('teacher_id')->unsigned()->index();
            $table->bigInteger('voter_id')->unsigned()->nullable();
            $table->string('voter_type'); // TODO: enum candidate?

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers_votes');
    }
}
