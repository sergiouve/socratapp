<?php

use App\Http\Controllers\CoursesController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ProposalsController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\TeachersController;

// Login routes
Route::post('/login', [LoginController::class, 'login'])->name('login.login');

// Courses routes
Route::get('/courses', [CoursesController::class, 'list'])->name('courses.list');
Route::post('/courses/{course}/votes', [CoursesController::class, 'vote'])->name('courses.votes.create');
Route::get('/courses/{course}/votes', [CoursesController::class, 'getVotes'])->name('courses.votes.get');

// Teachers routes
Route::get('/teachers', [TeachersController::class, 'list'])->name('teachers.get');
Route::post('/teachers/{teacher}/votes', [TeachersController::class, 'vote'])->name('teachers.votes.create');
Route::get('/teachers/{teacher}/votes', [TeachersController::class, 'getVotes'])->name('teachers.votes.get');

// Misc routes
Route::post('/proposals', [ProposalsController::class, 'create'])->name('proposals.list');
Route::get('/status', [StatusController::class, 'status'])->name('status.status');
