require('./bootstrap')

const listeners = require('./listeners')
const loaders = require('./loaders')
const helpers = require('./helpers')

window.addEventListener('load', boot, false)

async function boot() {
    loaders.loadCourses().then(() => {
        helpers.renderVisibleElements()
        listeners.init()
    })
}
