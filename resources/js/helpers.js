function toggleTeacherInfo(teacher = '', shouldBeVisible) {
    const teacherInfo = document.querySelector('.js-proposals')
    const identityBox = document.querySelector('.js-identity')
    const teacherSpan = document.querySelector('.js-teacher-name')
    const anonymizeBox = document.querySelector('.js-anonymize')

    teacherSpan.innerHTML = teacher

    if (shouldBeVisible) {
        teacherInfo.style.display = 'block'
        identityBox.style.display = 'none'
        anonymizeBox.style.display = 'block'
    } else {
        teacherInfo.style.display = 'none'
        identityBox.style.display = 'block'
        anonymizeBox.style.display = 'none'
    }
}

export function renderVisibleElements() {
    const teacher = window.localStorage.getItem('socratapp.teacherEmail')
    !! teacher && toggleTeacherInfo(teacher, true)
}
