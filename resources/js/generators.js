export function renderCourse(course) {
    const boxDiv = document.createElement('div')
    boxDiv.classList = 'card mb-2'

    const boxBody = document.createElement('div')
    boxBody.classList = 'card-body'

    const boxTitle = document.createElement('h3')
    boxTitle.innerHTML = course.title

    const boxSubtitle = document.createElement('h6')
    boxSubtitle.classList = 'font-italic'
    boxSubtitle.innerHTML = `Proposed by ${course.proposer.email}`

    const votes = document.createElement('p')
    votes.innerHTML = `Votes: ${course.votes}`

    const candidatesTitle = document.createElement('h4')
    candidatesTitle.classList = 'mt-2'
    candidatesTitle.innerHTML = 'Candidates'

    const courseButton = document.createElement('input')
    courseButton.type = 'button'
    courseButton.value = 'Vote for this course'
    courseButton.classList = 'btn btn-primary js-course-vote-button'
    courseButton.setAttribute('course-id', course.id)

    const candidatesList = document.createElement('ul')
    course.candidates.forEach((candidate) => {
        const candidateElement = document.createElement('li')
        const candidateButton = document.createElement('input')

        candidateButton.type = 'button'
        candidateButton.value = 'Vote for this teacher'
        candidateButton.classList = 'btn btn-link ml-2 js-teacher-vote-button'
        candidateButton.setAttribute('teacher-id', candidate.id)

        candidateElement.innerHTML = `${candidate.email}, Votes: ${candidate.votes}`
        candidateElement.classList = 'list-group-item list-group-item-light'
        candidatesList.classList = 'list-group pl-0'
        candidateElement.appendChild(candidateButton)
        candidatesList.appendChild(candidateElement)
    })

    boxBody.appendChild(boxTitle)
    boxBody.appendChild(boxSubtitle)
    boxBody.appendChild(votes)
    boxBody.appendChild(courseButton)
    boxBody.appendChild(candidatesTitle)
    boxBody.appendChild(candidatesList)
    boxDiv.appendChild(boxBody)

    return boxDiv
}
