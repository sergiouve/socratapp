const generators = require('./generators');

export async function loadCourses() {
    const courses = (await window.axios.get('/api/courses')).data
    const coursesContainer = document.querySelector('.js-courses')
    const coursesSelect = document.querySelector('.js-existing-courses')

    courses.forEach((course) => {
        coursesContainer.append(generators.renderCourse(course))
    })
}
