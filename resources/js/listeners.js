const initAnonymizeListener = () => {
    document.querySelector('.js-anonymize-button').addEventListener('click', () => {
        window.localStorage.removeItem('socratapp.teacherToken')
        window.localStorage.removeItem('socratapp.teacherEmail')

        location.reload()
    })
}

const initVotingListeners = () => {
    return
}

const initIdentifierListener = () => {
    const emailInput = document.querySelector('.js-email-input')
    const emailButton = document.querySelector('.js-email-button')

    emailButton.addEventListener('click', async () => {
        window.axios.post('/api/login', { email: emailInput.value })
              .then((response) => {
                  console.log(response);
                  window.localStorage.setItem('socratapp.teacherToken', response.data.token)
                  window.localStorage.setItem('socratapp.teacherEmail', response.data.forEmail)
                  window.axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token}`

                  location.reload()
              })
              .catch((error) => {
                  window.flash(error.response.data.message)
              })
    })
}

const initProposalListener = () => {
    const proposalInput = document.querySelector('.js-proposal-input')
    const proposalButton = document.querySelector('.js-proposal-button')

    proposalButton.addEventListener('click', () => {
        window.axios.post('/api/proposals', { courseTitle: proposalInput.value })
              .then(() => {
                  location.reload()
              })
              .catch((error) => {
                  window.flash(error.response.data.error)
              })
    })
}

const initVoteButtonsListeners = () => {
    const courseButtons = document.querySelectorAll('.js-course-vote-button');
    const teacherButtons = document.querySelectorAll('.js-teacher-vote-button');

    courseButtons.forEach((button) => {
        button.addEventListener('click', () => {
            const courseId = button.getAttribute('course-id')
            window.axios.post(`/api/courses/${courseId}/votes`)
                  .then(() => {
                      location.reload()
                  })
                  .catch((error) => {
                      window.flash(error.response.data.error)
                  })
        })
    })

    teacherButtons.forEach((button) => {
        button.addEventListener('click', () => {
            const teacherId = button.getAttribute('teacher-id')
            window.axios.post(`/api/teachers/${teacherId}/votes`)
                  .then(() => {
                      location.reload()
                  })
                  .catch((error) => {
                      window.flash(error.response.data.error)
                  })
        })
    })
}

export function init() {
    initAnonymizeListener()
    initVotingListeners()
    initIdentifierListener()
    initProposalListener()
    initVoteButtonsListeners()
}
