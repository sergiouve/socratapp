<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatusTest extends TestCase
{
    public function test_it_returns_the_status(): void
    {
        $response = $this->json('GET', '/api/status');
        $response->assertStatus(200);
    }
}
