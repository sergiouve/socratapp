<?php

namespace Tests\Feature;

use App\Models\Teacher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_returns_a_valid_token_for_non_existing_teachers(): void
    {
        $email = 'teacher@socratapp.org';
        $response = $this->json('POST', '/api/login', [ 'email' => $email ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('teachers_teachers', [
            'email' => $email,
        ]);
    }

    public function test_it_returns_a_valid_token_for_already_existing_teachers(): void
    {
        $teacher = Teacher::factory()->create();
        $response = $this->json('POST', '/api/login', [ 'email' => $teacher->email ]);

        $response->assertStatus(201);
        $this->assertDatabaseCount('auth_tokens', 1);
        $this->assertDatabaseHas('teachers_teachers', [
            'email' => $teacher->email,
        ]);
    }

    public function test_it_does_not_return_a_valid_token_for_invalid_emails(): void
    {
        $invalidEmail = 'THIS_IS_CLEARLY_NOT_AN_EMAIL';
        $response = $this->json('POST', '/api/login', [ 'email' => $invalidEmail ]);

        $response->assertStatus(422);
        $this->assertDatabaseCount('auth_tokens', 0);
    }

    public function test_it_does_not_create_new_teachers_on_already_existing_token_retrieval(): void
    {
        $teacher = Teacher::factory()->create();
        $firstResponse = $this->json('POST', '/api/login', [ 'email' => $teacher->email ]);
        $secondResponse = $this->json('POST', '/api/login', [ 'email' => $teacher->email ]);

        $firstResponse->assertStatus(201);
        $secondResponse->assertStatus(201);
        $this->assertDatabaseCount('auth_tokens', 2);
        $this->assertDatabaseCount('teachers_teachers', 1);
    }
}
