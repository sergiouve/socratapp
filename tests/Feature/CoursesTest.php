<?php

namespace Tests\Feature;

use App\Models\Teacher;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CoursesTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_returns_the_list_of_all_existing_courses(): void
    {
        $courses = Course::factory()->count(5)->create();

        $response = $this->json('GET', '/api/courses');

        $response->assertJsonCount(5);
    }

    public function test_it_invalidates_cache_on_new_proposal_creation(): void
    {
        $courses = Course::factory()->count(4)->create();
        $teacher = Teacher::factory()->create();

        $this->actAs($teacher);

        $beforeFlushResponse = $this->json('GET', '/api/courses');
        $this->json('POST', '/api/proposals', [ 'courseTitle' => 'Such a very cool course WOW' ]);
        $afterFlushResponse = $this->json('GET', '/api/courses');

        $beforeFlushResponse->assertJsonCount(4);
        $afterFlushResponse->assertJsonCount(5);
    }

    public function test_it_invalidates_cache_on_new_vote_creation(): void
    {
        $courses = Course::factory()->count(4)->create();
        $teacher = Teacher::factory()->create();

        $this->actAs($teacher);

        $beforeFlushResponse = $this->json('GET', '/api/courses');
        $beforeFlushContent = $this->parse($beforeFlushResponse);
        $this->json('POST', "/api/courses/{$courses[0]->id}/votes");
        $afterFlushResponse = $this->json('GET', '/api/courses');
        $afterFlushContent = $this->parse($afterFlushResponse);

        $this->assertEquals(0, $beforeFlushContent[0]['votes']);
        $this->assertEquals(1, $afterFlushContent[0]['votes']);
    }

    public function test_it_allows_regular_users_to_vote_for_courses(): void
    {
        $course = Course::factory()->create();

        $response = $this->json('POST', "/api/courses/{$course->id}/votes");

        $response->assertStatus(201);
        $this->assertDatabaseHas('courses_votes', [
            'course_id'  => $course->id,
            'voter_id'   => null,
            'voter_type' => 'REGULAR',
        ]);
    }

    public function test_it_allows_teachers_to_vote_for_courses(): void
    {
        $teacher = Teacher::factory()->create();
        $course = Course::factory()->create();

        $this->actAs($teacher);

        $response = $this->json('POST', "/api/courses/{$course->id}/votes");

        $response->assertStatus(201);
        $this->assertDatabaseHas('courses_votes', [
            'course_id'  => $course->id,
            'voter_id'   => $teacher->id,
            'voter_type' => 'TEACHER',
        ]);
    }

    public function test_it_does_not_allow_a_teacher_to_vote_for_a_course_more_than_once(): void
    {
        $teacher = Teacher::factory()->create();
        $course = Course::factory()->create();

        $this->actAs($teacher);

        $response = $this->json('POST', "/api/courses/{$course->id}/votes");
        $secondResponse = $this->json('POST', "/api/courses/{$course->id}/votes");

        $secondResponse->assertStatus(422);
    }
}
