<?php

namespace Tests\Feature;

use App\Models\Teacher;
use App\Models\Course;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProposalsTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_allows_to_create_proposals(): void
    {
        $teacher = Teacher::factory()->create();

        $this->actAs($teacher);

        $response = $this->json('POST', '/api/proposals', [ 'courseTitle' => 'Such a very cool course WOW' ]);
        $response->assertStatus(201);
        $this->assertDatabaseHas('proposals_proposals', [
            'candidate_id' => $teacher->id,
        ]);
    }

    public function test_proposals_to_non_existing_courses_create_new_courses(): void
    {
        $teacher = Teacher::factory()->create();

        $this->actAs($teacher);

        $this->assertDatabaseCount('courses_courses', 0);

        $response = $this->json('POST', '/api/proposals', [ 'courseTitle' => 'Such a very cool course WOW' ]);

        $response->assertStatus(201);
        $this->assertDatabaseCount('courses_courses', 1);
    }

    public function test_proposals_to_existing_courses_does_not_create_new_courses(): void
    {
        $course = Course::factory()->create();
        $teacher = Teacher::factory()->create();

        $this->actAs($teacher);

        $this->assertDatabaseCount('courses_courses', 1);

        $response = $this->json('POST', '/api/proposals', [ 'courseTitle' => $course->title ]);

        $response->assertStatus(201);
        $this->assertDatabaseCount('courses_courses', 1);
    }
}
