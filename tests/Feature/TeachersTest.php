<?php

namespace Tests\Feature;

use App\Models\Teacher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TeachersTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_allows_regular_users_to_vote_for_teachers(): void
    {
        $teacher = Teacher::factory()->create();

        $response = $this->json('POST', "/api/teachers/{$teacher->id}/votes");

        $response->assertStatus(201);
        $this->assertDatabaseHas('teachers_votes', [
            'teacher_id' => $teacher->id,
            'voter_id'   => null,
            'voter_type' => 'REGULAR',
        ]);
    }

    public function test_it_allows_teachers_to_vote_for_teachers(): void
    {
        $voter = Teacher::factory()->create();
        $voted = Teacher::factory()->create();

        $this->actAs($voter);

        $response = $this->json('POST', "/api/teachers/{$voted->id}/votes");

        $response->assertStatus(201);
        $this->assertDatabaseHas('teachers_votes', [
            'teacher_id' => $voted->id,
            'voter_id'   => $voter->id,
            'voter_type' => 'TEACHER',
        ]);
    }

    public function test_it_does_not_allow_a_teacher_to_vote_for_a_teacher_more_than_once(): void
    {
        $voter = Teacher::factory()->create();
        $voted = Teacher::factory()->create();

        $this->actAs($voter);

        $response = $this->json('POST', "/api/teachers/{$voted->id}/votes");
        $secondResponse = $this->json('POST', "/api/teachers/{$voted->id}/votes");

        $secondResponse->assertStatus(422);
    }
}
