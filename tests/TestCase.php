<?php

namespace Tests;

use App\Models\Teacher;
use App\Models\Token;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Testing\TestResponse;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function actAs(Teacher $teacher)
    {
        $token = Token::createFor($teacher)->token;

        $this->defaultHeaders = [
            'Authorization' => 'Bearer ' . $token,
        ];
    }

    protected function parse(TestResponse $response) : array
    {
        return json_decode($response->getContent(), true);
    }
}
