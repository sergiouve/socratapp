# Socratapp
Yes!

![IMAGE](https://imgs.xkcd.com/comics/final_exam.png )

## Prerequisites
It is supposed to be run using `docker-compose`

## Installation
```bash
# this might take a while the first time.
# wait until the message "INFO success: php-fpm entered RUNNING state, process has stayed up for > than 1 seconds (startsecs)" pops up
$ docker-compose up --build
```

Now the application is available at `localhost:8080`

## Running the Tests
```bash
$ docker-compose exec app composer run test
```

## Running static code analysis
```bash
$ docker-compose exec app composer run analyse
```
