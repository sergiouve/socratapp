<?php

namespace App\Providers;

use App\Events\ProposalCreated;
use App\Events\VoteCreated;
use App\Listeners\FlushCoursesCache;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ProposalCreated::class => [
            FlushCoursesCache::class,
        ],
        VoteCreated::class => [
            FlushCoursesCache::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
