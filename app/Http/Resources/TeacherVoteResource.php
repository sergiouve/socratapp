<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeacherVoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'teacherId' => $this->resource->teacher_id,
            'voterId'   => $this->resource->voter_id,
            'voterType' => $this->resource->voter_type,
        ];
    }
}
