<?php

namespace App\Http\Resources;

use App\Http\Resources\TeacherResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->resource->id,
            'title'         => $this->resource->title,
            'slug'          => $this->resource->slug,
            'proposer'      => new TeacherResource($this->proposer),
            'votes'         => $this->resource->votes()->count(),
            'candidates'    => TeacherResource::collection($this->getCandidates()),
        ];
    }
}
