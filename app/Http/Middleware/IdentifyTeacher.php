<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;

class IdentifyTeacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $teacher = Token::where('token', $request->bearerToken())->first()->teacher ?? null;
        $request->request->add(['locals.teacher' => $teacher]);

        return $next($request);
    }
}
