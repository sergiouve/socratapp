<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseVote;
use App\Http\Resources\CourseResource;
use App\Http\Resources\CourseVoteResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CoursesController
{
    public function list()
    {
        return Cache::rememberForever('courses:list', function () {
            return CourseResource::collection(Course::all())->jsonSerialize();
        });
    }

    public function vote(Request $request, Course $course)
    {
        $teacher = $request['locals.teacher'];

        if (! is_null($teacher) && $teacher->hasAlreadyVotedForCourse($course)) {
            return response()->json([ 'error' => 'teacher_has_already_voted_for_this_course' ], 422);
        }

        $vote = CourseVote::create([
            'course_id'     => $course->id,
            'voter_id'      => $teacher->id ?? null,
            'voter_type'    => is_null($teacher) ? 'REGULAR' : 'TEACHER',
        ]);

        return new CourseVoteResource($vote);
    }
}
