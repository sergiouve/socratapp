<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Models\Token;
use App\Http\Resources\TokenResource;
use Illuminate\Http\Request;

class LoginController
{
    public function login(Request $request)
    {
        $request->validate([ 'email' => 'required|email' ]);

        $teacher = Teacher::findOrCreate($request->email);
        $token = Token::createFor($teacher);

        return new TokenResource($token);
    }
}
