<?php

namespace App\Http\Controllers;

use App\Models\Teacher;
use App\Models\TeacherVote;
use App\Http\Resources\TeacherVoteResource;
use Illuminate\Http\Request;

class TeachersController
{
    public function vote(Request $request, Teacher $teacher)
    {
        $voterTeacher = $request['locals.teacher'];

        if (! is_null($voterTeacher) && $voterTeacher->hasAlreadyVotedForTeacher($teacher)) {
            return response()->json([ 'error' => 'teacher_has_already_voted_for_this_teacher' ], 422);
        }

        $vote = TeacherVote::create([
            'teacher_id'    => $teacher->id,
            'voter_id'      => $voterTeacher->id ?? null,
            'voter_type'    => is_null($voterTeacher) ? 'REGULAR' : 'TEACHER',
        ]);

        return new TeacherVoteResource($vote);
    }
}
