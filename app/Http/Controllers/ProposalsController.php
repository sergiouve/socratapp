<?php

namespace App\Http\Controllers;

use App\Models\Proposal;
use App\Models\Course;
use App\Http\Resources\ProposalResource;
use Illuminate\Http\Request;

class ProposalsController
{
    public function create(Request $request)
    {
        $teacher = $request['locals.teacher'];

        if (is_null($teacher)) {
            return response()->json([ 'error' => 'only_teachers_are_allowed_to_create_proposals' ], 403);
        }

        $request->validate([ 'courseTitle' => 'required|string' ]);

        $course = Course::findOrCreate($request->courseTitle, $teacher);

        if ($teacher->hasAlreadySentProposalFor($course)) {
            return response()->json([ 'error' => 'teacher_has_already_sent_proposal_for_this_course' ], 422);
        }

        $proposal = Proposal::create([
            'candidate_id' => $teacher->id,
            'course_id'    => $course->id,
        ]);

        return new ProposalResource($proposal);
    }
}
