<?php

namespace App\Http\Controllers;

class StatusController
{
    public function status()
    {
        return response()->json([ 'status' => 'OK' ]);
    }
}
