<?php

namespace App\Models;

use App\Models\Teacher;
use App\Models\Course;
use App\Events\ProposalCreated;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = 'proposals_proposals';

    protected $fillable = [
        'candidate_id', 'course_id',
    ];

    protected $dispatchesEvents = [
        'created' => ProposalCreated::class,
    ];

    public function candidate(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }
}
