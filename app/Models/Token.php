<?php

namespace App\Models;

use App\Models\Teacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Token extends Model
{
    protected $table = 'auth_tokens';

    protected $fillable = [
        'teacher_id', 'token', 'is_active',
    ];

    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    public static function createFor(Teacher $teacher): Token
    {
        self::deactivateOldTokensFor($teacher);

        return self::create([
            'teacher_id' => $teacher->id,
            'token'      => self::generate(),
            'is_active'  => true,
        ]);
    }

    public static function generate(): string
    {
        return md5(rand(1, 10) . microtime());
    }

    public static function deactivateOldTokensFor(Teacher $teacher)
    {
        self::where('teacher_id', $teacher->id)->update(['is_active' => false]);
    }
}
