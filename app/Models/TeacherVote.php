<?php

namespace App\Models;

use App\Events\VoteCreated;
use Illuminate\Database\Eloquent\Model;

class TeacherVote extends Model
{
    protected $table = 'teachers_votes';

    protected $fillable = [
        'teacher_id', 'voter_id', 'voter_type',
    ];

    protected $dispatchesEvents = [
        'created' => VoteCreated::class,
    ];
}
