<?php

namespace App\Models;

use App\Models\Token;
use App\Models\Course;
use App\Models\TeacherVote;
use App\Models\Proposal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Teacher extends Model
{
    use HasFactory;

    protected $table = 'teachers_teachers';

    protected $fillable = [
        'email',
    ];

    public function tokens(): HasMany
    {
        return $this->hasMany(Token::class, 'teacher_id', 'id');
    }

    public function votes(): HasMany
    {
        return $this->hasMany(TeacherVote::class);
    }

    public static function findOrCreate(string $email): Teacher
    {
        return self::where('email', $email)->first() ?? self::create([ 'email' => $email ]);
    }

    public function hasAlreadyVotedForCourse(Course $course): bool
    {
        return (bool) $course->votes()->where('voter_id', $this->id)->count();
    }

    public function hasAlreadyVotedForTeacher(Teacher $teacher): bool
    {
        return (bool) $teacher->votes()->where('voter_id', $this->id)->count();
    }

    public function hasAlreadySentProposalFor(Course $course): bool
    {
        return (bool) Proposal::where([
            'candidate_id' => $this->id,
            'course_id'    => $course->id,
        ])->count();
    }
}
