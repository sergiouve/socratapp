<?php

namespace App\Models;

use App\Events\VoteCreated;
use Illuminate\Database\Eloquent\Model;

class CourseVote extends Model
{
    protected $table = 'courses_votes';

    protected $fillable = [
        'course_id', 'voter_id', 'voter_type',
    ];

    protected $dispatchesEvents = [
        'created' => VoteCreated::class,
    ];
}
