<?php

namespace App\Models;

use App\Models\Teacher;
use App\Models\CourseVote;
use App\Models\Proposal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Str;

class Course extends Model
{
    use HasFactory;

    protected $table = 'courses_courses';

    protected $fillable = [
        'proposer_id', 'title', 'slug',
    ];

    public function votes(): HasMany
    {
        return $this->hasMany(CourseVote::class);
    }

    public function proposer(): HasOne
    {
        return $this->hasOne(Teacher::class, 'id', 'proposer_id');
    }

    public function getCandidates()
    {
        return Proposal::where('course_id', $this->id)->get()->map(fn ($proposal) => $proposal->candidate);
    }

    public static function generateSlug(string $title): string
    {
        return Str::snake(Str::lower($title));
    }

    public static function findOrCreate(string $title, Teacher $teacher): Course
    {
        return self::where('slug', self::generateSlug($title))->first() ??
            self::create([
            'title'         => $title,
            'slug'          => self::generateSlug($title),
            'proposer_id'   => $teacher->id,
        ]);
    }
}
