<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Cache;

class FlushCoursesCache
{
    public function handle($event)
    {
        Cache::forget('courses:list');
    }
}
