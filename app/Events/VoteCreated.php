<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;

class VoteCreated
{
    public $vote;

    public function __construct(Model $vote)
    {
        $this->vote = $vote;
    }
}
