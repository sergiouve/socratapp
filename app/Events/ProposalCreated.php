<?php

namespace App\Events;

use App\Models\Proposal;

class ProposalCreated
{
    public $proposal;

    public function __construct(Proposal $proposal)
    {
        $this->proposal = $proposal;
    }
}
